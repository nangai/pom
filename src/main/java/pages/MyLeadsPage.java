package pages;

import wrappers.Annotations;

public class MyLeadsPage extends Annotations{

	public CreateLeadPage selectCreateLead()
	{
		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadPage();
	}
}

package pages;

import wrappers.Annotations;

public class ViewLeadPage extends Annotations{

	public ViewLeadPage verifyLoginName(String data) {
		String loginName = driver.findElementByTagName("h2").getText();
		if(loginName.contains(data)) {
			System.out.println("Login success");
		}else {
			System.out.println("Logged username mismatch");
		}
		return this;
	}
}

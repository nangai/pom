package pages;

import wrappers.Annotations;

public class CreateLeadPage extends Annotations{

	public CreateLeadPage enterDetails() {
		driver.findElementById("createLeadForm_companyName").sendKeys("TCS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Nangai");
		driver.findElementById("createLeadForm_lastName").sendKeys("Arun");
		driver.findElementByName("submitButton").click();
		return this;
	}
}
